import Context from '.';

let context;
const container = { notifyDragFinished: () => {} }

describe('#registerDragStart()', () => {
    beforeEach(() => {
        context = new Context();
    })

    it('returns unique drag id', () => {
        const idsArray = Array.from(Array(200)).map(() => {
            return context.registerDragStart(container, 'jsxElement' ,'draggableId')
        });
        const uniqueElementsCount = (new Set(idsArray)).size; //lol size...
        
        expect(uniqueElementsCount).toEqual(idsArray.length)
    })

    it('persists drag data in internal storage', () => {
        const uniqueId = context.registerDragStart(container, 'jsxElement', 'draggableId')

        expect(context.__storage).toEqual({sourceContainer: container, jsxElement:'jsxElement', draggableId: 'draggableId', dragId: uniqueId})
    })
})

describe('#registerDragFinish()', () => {
    let dragId;
    beforeEach(() => {
        context = new Context();
        dragId = context.registerDragStart(container, 'jsxElement', 'draggableId');
    })

    describe('when called with invalid dragId', ()=> {
        it('returns null', () => {
            const jsxElement = context.registerDragFinish(null);
            expect(jsxElement).toEqual(null);
        })

        it('returns null', () => {
            const jsxElement = context.registerDragFinish(undefined);
            expect(jsxElement).toEqual(null);
        })

        it('returns null', () => {
            const jsxElement = context.registerDragFinish('wrong id');
            expect(jsxElement).toEqual(null);
        })

        xit('does\'t touch source container', () => {

        })
    })

    describe('when called with valid dragId', ()=> {
        it('returns jsx token of dragged element', () => {
            expect(context.registerDragFinish(dragId)).toEqual('jsxElement');
        })

        it('notifies source container that dragged element should be removed', () => {
            const spy = spyOn(container, 'notifyDragFinished');
            const dragId = context.registerDragStart(container, 'jsxElement', 'draggableId');
            
            context.registerDragFinish(dragId);

            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith('draggableId');
        })
    })    
})
