import { uniqueId } from 'lodash';

export default class DradContext {
    __storage = {};

    registerDragStart(sourceContainer, jsxElement, draggableId) {
      const uniqueDragId = uniqueId('dragId-');
      this.__storage = {
        sourceContainer, draggableId, jsxElement, dragId: uniqueDragId,
      };
      return uniqueDragId;
    }

    registerDragFinish(uniqueDragId) {
      const { dragId: storedDragId, jsxElement } = this.__storage;

      if ((!storedDragId) || (storedDragId !== uniqueDragId)) {
        return null;
      }
      this.__notifySourceContainerOnDragFinish();
      return jsxElement;
    }

    // private

    __notifySourceContainerOnDragFinish() {
      const { draggableId, sourceContainer } = this.__storage;
      sourceContainer.notifyDragFinished(draggableId);
    }
}
