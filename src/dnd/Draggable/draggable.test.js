import React from 'react';
import ReactDOM from 'react-dom';
import Draggable from './index';
import { act } from 'react-dom/test-utils';

describe('#getCoordinates()', () => {
    let container;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        }
    );
          
    afterEach(() => {
        document.body.removeChild(container);
        container = null;
        }
    );

    it(' return correct position', () => {
        const draggableRef = React.createRef()
        
        const dom = (
            <>
            <div id='div1' style={{height: "200px", width: "400px"}}></div>
            <Draggable ref={draggableRef}>
                <div id="div2" style={{height: "100px", width: "500px"}}></div>
            </Draggable>
            </>
        );
        
        act(() => {
            ReactDOM.render(dom, container);
        })
        
        const coordinates = draggableRef.current.getCoordinates();

        expect(coordinates).toEqual({x: 0, y: 0 })
    });
})