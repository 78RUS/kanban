import React from 'react';

export default class Draggable extends React.Component {
    state = {
      domNodeRef: React.createRef(),
    }

    dragStart = (e) => {
      const { container, draggableId } = this.props;
      const dragId = container.registerDragStart(draggableId);
      e.dataTransfer.setData('text/plain', dragId);
    }

    // noAllowDrop = (e) => {
    //   e.stopPropagation();
    //   return false;
    // }

    getCoordinates = () => {
      const { domNodeRef } = this.state;
      const box = domNodeRef.current.getBoundingClientRect();
      const middleX = box.left + (box.width / 2);
      const middleY = box.top + (box.height / 2);
      return { y: box.top, x: box.left, middleX, middleY };
    }

    render() {
      const { domNodeRef } = this.state;
      const {
        id, className, style, children,
      } = this.props;
      return (
        <div ref={domNodeRef} id={id} className={className} style={style} draggable="true" onDragStart={this.dragStart}>
          {children}
        </div>
      );
    }
}
