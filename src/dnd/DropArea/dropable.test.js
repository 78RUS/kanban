import React from 'react';
import ReactDOM from 'react-dom';
import DropArea from './index';


describe('Droppable stuff', () => {
    let container;
    const droppableRef = React.createRef();
    const dom = <DropArea ref={droppableRef} />;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        ReactDOM.render(dom, document.createElement('div'));
    });
          
    afterEach(() => {
        document.body.removeChild(container);
        container = null;
        }
    );

    describe('#removeElementByKey()', () => {
        it('removes an element at index', () => {
            const incomingCollection = [{key: 1}, {key: 2}, {key: 3}];
            const result = droppableRef.current.removeElementByKey(incomingCollection, 2);
            expect(result).toEqual([{key: 1}, {key: 3}]);
        })
    })

    describe('#findByKey()', () => {
        it('removes an element at index', () => {
            const incomingCollection = [{key: 1}, {key: 2}, {key: 3}];
            const result = droppableRef.current.findByKey(incomingCollection, 2);
            expect(result).toEqual({key: 2});
        })
    })

    describe('#insertAt()', () => {
        const testArray = [1, 2, 3]
        it('inserts at the begining of the list', () => {
            const position = 0;
            const element = 100;
            const result = droppableRef.current.insertAt(testArray, element, position);
            expect(result).toEqual([100,1,2,3]);
        })
        it('inserts in the middle of the list', () => {
            const position = 1;
            const element = 100;
            const result = droppableRef.current.insertAt(testArray, element, position);
            expect(result).toEqual([1,100,2,3]);

        })
        it('inserts at the end of the list', () => {
            const position = 5;
            const element = 100;
            const result = droppableRef.current.insertAt(testArray, element, position);
            expect(result).toEqual([1,2,3, 100]);

        })
    })
});