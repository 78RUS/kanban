import React from 'react';
import { uniqueId } from 'lodash';
import Draggable from '../Draggable';


export default class DropArea extends React.Component {
    state = {
      children: [],
    }

    constructor(props) {
      super(props);
      const children = React.Children.map(props.children, c => c);
      if (!children) {
        this.state.children = [];
      } else {
        this.state.children = children.map(this.packChild);
      }
    }

    // DROP LOGIC

    getDragId = e => e.dataTransfer.getData('text/plain');

    shouldAcceptDrop(e) {
      const dragId = this.getDragId(e);
      if (!dragId) {
        return false;
      }
      return true;
    }

    acceptDrop(e) {
      const dragId = this.getDragId(e);
      this.appendDroppedElementToContainer(dragId, e);
    }

    appendDroppedElementToContainer = (dragId, e) => {
      const coordX = e.clientX;
      const coordY = e.clientY;
      this.setState((state, props) => {
        const { children } = state;
        const childRefs = children.map(elem => elem.ref);
        const insertPosition = this.calculateInsertPosition({ dropX: coordX, dropY: coordY }, childRefs);

        const { dragContext } = props;
        const droppedJsxElement = dragContext.registerDragFinish(dragId);
        const jsxToAppend = React.cloneElement(droppedJsxElement);
        const childToAppend = this.packChild(jsxToAppend);

      
        // const { children } = state;
        const childrenAndInsertedElement = this.insertAt(children, childToAppend, insertPosition);
        return { children: childrenAndInsertedElement };
      });
    }

    // DROP API

    drop = (e) => {
      e.preventDefault();
      if (this.shouldAcceptDrop(e)) {
        this.acceptDrop(e);
      }
    }

    // HELPER TO EXTRACT TO SEP MODULE

    findByKey = (collection, key) => collection.find(hash => hash.key === key)

    insertAt = (collection, element, position) => {
      const before = collection.slice(0, position);
      const after = collection.slice(position);
      return [...before, element, ...after];
    }

    removeElementByKey = (childList, key) => childList.filter(hash => hash.key !== key)

    // DROP LOGIC

    calculateInsertPosition = (dropCoordinates, refCollection) => {
      const { dropY } = dropCoordinates;

      if (!refCollection) {
        return 0;
      }

      const indexOfElementUnderDropPoint = refCollection.findIndex((ref) => {
        const { middleY } = ref.current.getCoordinates();
        return dropY <= middleY;
      });

      switch (indexOfElementUnderDropPoint) {
        case -1:
          return refCollection.length;
        default:
          return indexOfElementUnderDropPoint;
      }
    }

    allowDrop = (e) => {
      e.preventDefault();
      return true;
    }

    // CONTAINER API

    appendChild = (child) => {
      const { children } = this.state;
      const packedChild = this.packChild(child);
      this.setState({ children: [...children, packedChild] });
    }

    registerDragStart = (draggableId) => {
      const { dragContext } = this.props;
      const { children } = this.state;
      const { jsxElement } = this.findByKey(children, draggableId);
      return dragContext.registerDragStart(this, jsxElement, draggableId);
    }

    notifyDragFinished = (draggableId) => {
      this.setState((state) => {
        const { children } = state;
        const newChildren = this.removeElementByKey(children, draggableId);
        return { children: newChildren };
      });
    }

    // INTERNAL API

    packChild = jsxElement => ({ jsxElement, ref: React.createRef(), key: uniqueId('draggable-') });

    // React API

    render() {
      const { id, className, style } = this.props;
      const { children } = this.state;
      return (
        <div id={id} className={className} onDrop={this.drop} onDragOver={this.allowDrop} style={style}>
          {children.map(hash => <Draggable key={hash.key} draggableId={hash.key} ref={hash.ref} container={this}>{hash.jsxElement}</Draggable>)}
        </div>
      );
    }
}
