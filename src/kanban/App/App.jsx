import React from 'react';
import './App.css';
import Board from '../Board/Board';
import Column from '../Column/Column';
import Card from '../Card/Card';
import DragContext from '../../dnd/DragContext';

function App() {
  const dragContext = new DragContext();
  return (
    <div className="App">
      <Board dragContext={dragContext}>
        <Column columnHeaderContent="План на месяц" dragContext={dragContext}>
          <Card>Пройти курс по React</Card>
          <Card>Отметить день рождения</Card>
          <Card>Собрать подборку вакансий для отклика.</Card>
          <Card>Записаться на курсы английского языка, чтобы уехать жить в Лондон</Card>
          <Card>Сделать бекенд своего сайта на node.js</Card>
          <Card>Собрать портфолио</Card>
          <Card>Написать первую статью в блог</Card>
          <Card>
            Записаться в мотошколу. Хотя немного страшновато, конечно.
            Друзья и родители против, но очень хочется. Но кого я обманываю,
            уже 2 года решаюсь на этот шаг 😢 Еще и друзья будут хрустиком называть.
            В общем, хотя бы подумать над этим.
          </Card>
          <Card>Нет, я серьезный человек, иду в мотошколу. Записываюсь!</Card>
          <Card>Записаться на интенсив по React Native</Card>
          <Card>Уехать в отпуск. Отдыхать тоже надо!</Card>
        </Column>
        <Column columnHeaderContent="План на день" dragContext={dragContext}>
          <Card>Записаться на курс по React</Card>
          <Card>Забронировать тир на субботу</Card>
          <Card>Забронировать тир на субботу</Card>
          <Card>Накидать тем для статей в блог</Card>
        </Column>
        <Column columnHeaderContent="Итоги" dragContext={dragContext}>
          <Card>Сделать колонку Итоги</Card>
        </Column>
      </Board>
    </div>
  );
}

export default App;
