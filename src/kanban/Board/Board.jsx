import React from 'react';
import { uniqueId } from 'lodash';
import Column from '../Column/Column';
import './Board.post.css';
import plusIcon from './plus_icon.svg';
import crossIcon from './cross_icon.svg';
import DragContext from '../../dnd/DragContext';

export default class Board extends React.Component {
  state = {
    children: [],
    isNewColumnInputShown: false,
    newColumnContent: '',
    dragContext: new DragContext(),
  }

  constructor(props) {
    super(props);
    const children = React.Children.map(props.children, c => c);

    if (!children) {
      this.state.children = [];
    } else {
      this.state.children = children;
    }
    const { dragContext } = this.props;
    if (dragContext) {
      this.state.dragContext = dragContext;
    }
  }

  addColumnHandler = () => {
    this.setState({ isNewColumnInputShown: true });
  }

  discardColumnHandler = () => {
    this.setState({ isNewColumnInputShown: false });
  }

  newColumnContentInputChangeHandler = ({ target }) => {
    const { value } = target;
    this.setState({ newColumnContent: value });
  }

  submitColumnHandler = () => {
    const { newColumnContent, children, dragContext } = this.state;
    if (!newColumnContent || (newColumnContent.length === 0)) {
      return;
    }
    this.setState({ children: [...children, <Column dragContext={dragContext} columnHeaderContent={newColumnContent} key={uniqueId('column')} />] });
    this.setState({ newColumnContent: '' });
  }

  submitColumnOnEnterHandler = (e) => {
    if (e.keyCode === 13) {
      e.preventDefault();
      this.submitColumnHandler();
      this.setState({ newColumnContent: '' });
    }
  }

  notAllowDrop = (e) => {
    e.preventDefault();
    e.stopPropagation();
    return false;
  }

  renderBasePane() {
    return (
      <div className="Board-AddColumnPane">
        <span
          className="Board-AddColumnControl"
          onClick={this.addColumnHandler}
        >
          <img src={plusIcon} className="Board-AddColumnPlusIcon" alt="" />
          <span>Добавить еще одну колонку</span>
        </span>
      </div>
    );
  }

  renderActivePane() {
    const { newColumnContent } = this.state;
    return (
      <div className="Board-AddColumnPane">
        <input
          value={newColumnContent}
          type="text"
          className="Board-NewColumnContentInput"
          onChange={this.newColumnContentInputChangeHandler}
          placeholder="Введите название колонки"
          onKeyDown={this.submitColumnOnEnterHandler}
          onDrop={this.notAllowDrop}
          autoFocus
        />
        <div className="Board-SubmitColumnPane">
          <button
            className="Board-SubmitColumnControl"
            type="button"
            name="button"
            onClick={this.submitColumnHandler}
          >
              Добавить колонку
          </button>
          <img className="Column-DiscardColumnControl" src={crossIcon} onClick={this.discardColumnHandler} alt="" />
        </div>
      </div>
    );
  }

  renderAddColumnPane() {
    const { isNewColumnInputShown } = this.state;
    switch (isNewColumnInputShown) {
      case true:
        return this.renderActivePane();
      case false:
        return this.renderBasePane();
      default:
        throw new Error('Illegal state');
    }
  }

  renderNewColumnContentInput() {
    const { isNewColumnInputShown } = this.state;
    if (!isNewColumnInputShown) {
      return null;
    }
    const { newColumnContent } = this.state;
    return (
      <input
        value={newColumnContent}
        type="text"
        className="Board-NewColumnContentInput"
        onChange={this.newColumnContentInputChangeHandler}
        placeholder="Введите название колонки"
        onKeyDown={this.submitColumnOnEnterHandler}
        autoFocus
        required
      />
    );
  }

  render() {
    const { children } = this.state;
    return (
      <div className="Board">
        {children}
        {this.renderAddColumnPane()}
      </div>
    );
  }
}
