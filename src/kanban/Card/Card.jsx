import React from 'react';
import './Card.post.css';
import { uniqueId } from 'lodash';

export default class Card extends React.Component {
  render() {
    const { children } = this.props;
    return (
      <div className="Card" id={uniqueId('card')}>
        <span className="Card-Content">
          {children}
        </span>
      </div>
    );
  }
}
