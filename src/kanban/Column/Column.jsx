import React from 'react';
import { uniqueId } from 'lodash';
import Card from '../Card/Card';
import './Column.post.css';
import plusIcon from './plus_icon.svg';
import crossIcon from './cross_icon.svg';
import DropArea from '../../dnd/DropArea';

export default class Column extends React.Component {
  state = {
    container: React.createRef(),
    isNewCardInputShown: false,
    newCardContent: '',
  };

  addCardHandler = () => {
    this.setState({ isNewCardInputShown: true });
  }

  discardCardHandler = () => {
    this.setState({ isNewCardInputShown: false });
  }

  newCardContentInputChangeHandler = (e) => {
    const { value } = e.target;
    this.setState({ newCardContent: value });
  }

  submitCardHandler = () => {
    const { newCardContent } = this.state;
    if (!newCardContent || (newCardContent.length === 0)) {
      return;
    }
    const card = <Card>{newCardContent}</Card>;
    const { container } = this.state;
    container.current.appendChild(card);
    this.setState({ newCardContent: '' });
  }

  submitCardOnEnterHandler = (e) => {
    if (e.keyCode === 13) {
      e.preventDefault();
      this.submitCardHandler();
    }
  }

  notAllowDrop = (e) => {
    e.preventDefault();
    e.stopPropagation();
    return false;
  }

  renderBasePane() {
    return (
      <div className="Column-AddCardPane">
        <span
          className="Column-AddCardControl"
          onClick={this.addCardHandler}
        >
          <img src={plusIcon} className="Board-AddColumnPlusIcon" alt="" />
          <span>Добавить еще одну карточку</span>
        </span>
      </div>
    );
  }

  renderActivePane() {
    return (
      <div className="Column-AddCardPane">
        <button className="Column-SubmitCardControl" type="button" name="button" onClick={this.submitCardHandler}>Добавить карточку</button>
        <img className="Column-DiscardCardControl" src={crossIcon} onClick={this.discardCardHandler} alt="" />
      </div>
    );
  }

  renderAddCardPane() {
    const { isNewCardInputShown } = this.state;

    switch (isNewCardInputShown) {
      case true:
        return this.renderActivePane();
      case false:
        return this.renderBasePane();
      default:
        throw new Error('Illegal state');
    }
  }

  renderNewCardContentInput() {
    const { isNewCardInputShown } = this.state;
    if (!isNewCardInputShown) {
      return null;
    }
    const { newCardContent } = this.state;
    return (
      <div className="Column-NewCardContentInputPane">
        <textarea
          value={newCardContent}
          className="Column-NewCardContentInput"
          onChange={this.newCardContentInputChangeHandler}
          placeholder="Введите название карточки"
          onKeyDown={this.submitCardOnEnterHandler}
          onDrop={this.notAllowDrop}
          autoFocus
        />
      </div>
    );
  }

  render() {
    const { columnHeaderContent, children, dragContext } = this.props;
    const { container } = this.state;
    return (
      <div className="Column">
        <div className="Column-Header">
          {columnHeaderContent}
        </div>
        <DropArea dragContext={dragContext} ref={container} className="Column-List" id={uniqueId('Column-List')}>
          {children}
        </DropArea>
        <div className="Column-Footer">
          {this.renderNewCardContentInput()}
          {this.renderAddCardPane()}
        </div>
      </div>
    );
  }
}
